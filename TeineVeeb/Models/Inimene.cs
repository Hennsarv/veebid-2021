﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace TeineVeeb.Models
{
    public class Inimene
    {
        public static List<Inimene> Raffas = new List<Inimene>
        {
            new Inimene() {Nr = 1, Nimi = "Henn", Vanus = 66},
            new Inimene() {Nr = 2, Nimi = "Ants", Vanus = 40},
            new Inimene() {Nr = 3, Nimi = "Peeter", Vanus = 28},
        };

        public int Nr { get; set; }
        [Display(Name = "Person name")]
        public string Nimi { get; set; }
        [Display(Name = "Age")]
        public int Vanus { get; set; }

        public static Inimene GetInimene(int nr) 
            => Raffas.Where(x => x.Nr == nr).SingleOrDefault();

        public static void NewInimene(string nimi, int vanus)
            => Raffas.Add(new Inimene
            {
                Nr = Raffas.Select(x => x.Nr).Max() + 1,
                Nimi = nimi,
                Vanus = vanus
            });

    }
}