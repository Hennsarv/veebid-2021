﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeineVeeb.Models;

namespace TeineVeeb.Controllers
{
    public class InimeneController : Controller
    {
        // GET: Inimene
        public ActionResult Index()
        {
            return View(Inimene.Raffas);
        }

        // GET: Inimene/Details/5
        public ActionResult Details(int id)
        {
            return View(Inimene.GetInimene(id));
        }

        // GET: Inimene/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Inimene/Create
        [HttpPost]
        public ActionResult Create(Inimene inimene)
        {
            try
            {
                Inimene.NewInimene(inimene.Nimi, inimene.Vanus);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Inimene/Edit/5
        public ActionResult Edit(int id)
        {
            Inimene inimene = Inimene.GetInimene(id);
            if (inimene == null) return HttpNotFound();

            return View( inimene);
        }

        // POST: Inimene/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Inimene inimene)
        {

            try
            {
                Inimene.GetInimene(id).Vanus = inimene.Vanus;

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Inimene/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Inimene/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
